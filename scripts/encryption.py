import numpy as np
from operator import xor
import bitarray as ba
import random


class CyclicCode:
    def __init__(self, k, n, g, t):
        self.k = k
        self.n = n
        self.g = g
        self.t = t
        self.syndromes_table = []
        self.__added_bits = 0

    def generate_table_of_syndromes(self):
        self.syndromes_table.clear()
        for code in self.__generate_restricted_weight_codes(self.n, self.t):
            self.syndromes_table.append((self.remainder(code), code))

    def __generate_restricted_weight_codes(self, length, err_count):
        if length > 1:
            if err_count > 0:
                for code in self.__generate_restricted_weight_codes(length - 1, err_count - 1):
                    yield np.append(code, True)
                for code in self.__generate_restricted_weight_codes(length - 1, err_count):
                    yield np.append(code, False)
            else:
                yield np.array([False for _ in range(length)])
        else:
            if err_count > 0:
                yield np.array([True])
            yield np.array([False])

    def encode_non_sys(self, arr):
        result = np.array([False for _ in range(self.n)])
        for i, bit in enumerate(arr):
            if not bit:
                continue
            result[i:i + self.k] = xor(result[i:i + self.k], self.g)
        return result

    def encode_sys(self, arr):
        result = np.array(range(self.n), dtype=bool)
        result[self.n - self.k:] = arr
        ext_arr = np.array([False for _ in range(self.n)])
        ext_arr[self.n - self.k:] = arr
        result[:self.n - self.k] = self.remainder(ext_arr)
        return result

    def remainder(self, arr):
        result = np.copy(arr)
        for i in range(self.n, self.n - self.k, -1):
            if result[i - 1]:
                result[i - self.n + self.k - 1: i] = xor(result[i - self.n + self.k - 1: i], self.g)
        return result[:self.n - self.k]

    def encode_file(self, input_file, output_file):
        file_bytes = ba.bitarray()
        file_bytes.fromfile(input_file)
        file_list = file_bytes.tolist()
        rem = len(file_list) % self.k
        if rem != 0:
            self.__added_bits = self.k - rem
            file_list.extend(ba.bitarray(self.__added_bits))
        bit_arrays = np.array(file_list, dtype=bool).reshape(-1, self.k)
        result_array = ba.bitarray()
        for k_bits in bit_arrays:
            result_array.extend(self.encode_sys(k_bits))
        result_array.tofile(output_file)

    def decode_file(self, input_file, output_file):
        file_bytes = ba.bitarray()
        file_bytes.fromfile(input_file)
        file_list = file_bytes.tolist()
        rem = len(file_list) % self.n
        if rem != 0:
            file_list = file_list[:-rem]
        bit_arrays = np.array(file_list, dtype=bool).reshape(-1, self.n)
        result_array = ba.bitarray()
        for arr_index, n_bits in enumerate(bit_arrays):
            rem = self.remainder(n_bits)
            has_error = np.any(rem)
            if has_error:
                syndrome = self.__syndrome_by_reminder(rem)
                for index, bit in enumerate(syndrome):
                    if bit:
                        print('Found an error in the position:', arr_index * self.n + index)
                n_bits = xor(n_bits, syndrome)
            result_array.extend(n_bits[-self.k:])
        result_array = result_array[:-self.__added_bits]
        result_array.tofile(output_file)

    def __syndrome_by_reminder(self, rem):
        for reminder, syndrome in self.syndromes_table:
            if np.array_equal(reminder, rem):
                return syndrome
        raise ValueError('The syndrome was not found (perhaps the table of syndromes was not generated)')


def list_to_string(arr):
    result = ''
    for item in arr:
        result += '1' if item else '0'
    return result


def print_all_errors(code):
    for reminder, syndrome in code.syndromes_table:
        print(list_to_string(reminder), '->', list_to_string(syndrome))


def encode(code):
    with open('./data/file.txt', 'rb') as input_file:
        with open('./data/output.txt', 'wb') as output_file:
            code.encode_file(input_file, output_file)


def corrupt_file():
    file_bytes = ba.bitarray()
    with open('./data/output.txt', 'rb') as file:
        file_bytes.fromfile(file)
        random.seed()
        random_position = random.randint(0, len(file_bytes) - 1)
        print("Changed position:", random_position)
        file_bytes[random_position] = not file_bytes[random_position]
    with open('./data/output.txt', 'wb') as file:
        file_bytes.tofile(file)


def corrupt_file_double_error():
    file_bytes = ba.bitarray()
    with open('./data/output.txt', 'rb') as file:
        file_bytes.fromfile(file)
        random.seed()
        random_position = random.randint(0, len(file_bytes) - 1)
        print("Changed position:", random_position)
        file_bytes[random_position] = not file_bytes[random_position]
        random_position += 1
        print("Changed position:", random_position)
        file_bytes[random_position] = not file_bytes[random_position]
    with open('./data/output.txt', 'wb') as file:
        file_bytes.tofile(file)


def decode(code):
    with open('./data/output.txt', 'rb') as input_file:
        with open('./data/decoded.txt', 'wb') as output_file:
            code.decode_file(input_file, output_file)


if __name__ == '__main__':
    # cyclicCode = CyclicCode(4, 7, np.array([True, False, True, True]), 1)
    cyclicCode = CyclicCode(7, 15, np.array([True, False, False, False, True, False, True, True, True]), 2)
    cyclicCode.generate_table_of_syndromes()
    print_all_errors(cyclicCode)
    encode(cyclicCode)
    corrupt_file_double_error()
    decode(cyclicCode)
